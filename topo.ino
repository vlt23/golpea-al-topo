//int pinGolpe1 = 1;
const int pinGolpe2 = 2;
const int pinGolpe3 = 3;

//const int pinLed1 = 13;
const int pinLed2 = 12;
const int pinLed3 = 11;

volatile int randNumber;      // elegir que topo aparecer
volatile int golpeYN = 0;     // ver si se ha dado el golpe o no. Tras el golpe correcto, se pone a -1
unsigned long currentTime;    // controlar que el topo aparezca durante dos segundos si no hay ningun golpe
volatile int puntos = 0;

const unsigned long gameTime = 60000;   // 1 minuto de juego
const int pinHit = 9;
const int pinMiss = 8;


void setup() {
  //pinMode(pinGolpe1, INPUT_PULLUP);
  pinMode(pinGolpe2, INPUT_PULLUP);
  pinMode(pinGolpe3, INPUT_PULLUP);
  //pinMode(pinLed1, OUTPUT);
  pinMode(pinLed2, OUTPUT);
  pinMode(pinLed3, OUTPUT);

  randomSeed(analogRead(0));
  //attachInterrupt(digitalPinToInterrupt(pinGolpe1), golpeInterrupt, LOW);
  attachInterrupt(digitalPinToInterrupt(pinGolpe2), golpeInterrupt2, LOW);
  attachInterrupt(digitalPinToInterrupt(pinGolpe3), golpeInterrupt3, LOW);
  Serial.begin(9600);
}

void loop() {
  if (millis() > gameTime) {
    // LCD print("Se ha llegado acabado el tiempo de juego. Pulse el boton de Arduino para volver a jugar");
    Serial.println("Fin juego");
    Serial.print("Su puntuacion es: ");
    Serial.println(puntos);
  } else {
  randNumber = (int) random(2);
  Serial.println(randNumber);
  currentTime = millis();
  while (!golpeYN && millis() - 2000 < currentTime) {
    /*if (randNumber == 0) {
      digitalWrite(pinLed1, HIGH);
    }*/
    if (randNumber == 0) {
      digitalWrite(pinLed2, HIGH);
    }
    else if (randNumber == 1) {
      digitalWrite(pinLed3, HIGH);
    }
    else {
      Serial.begin("No deberia entrar aqui");
    }
  }
  //digitalWrite(pinLed1, LOW);
  digitalWrite(pinLed2, LOW);
  digitalWrite(pinLed3, LOW);
  golpeYN = 0;
  digitalWrite(pinHit, LOW);
  digitalWrite(pinMiss, LOW);
  randNumber = -1;
  delay(700);
  }
}

void golpeInterrupt2() {
  delay(700);
  if (randNumber == 0) {
    puntos++;
    golpeYN = 1;
    digitalWrite(pinHit, HIGH);
    Serial.println("Hit 1!");
  } else {
    Serial.println("Miss...");
  }
}

void golpeInterrupt3() {
  delay(700);
  if (randNumber == 1) {
    puntos++;
    golpeYN = 1;
    digitalWrite(pinHit, HIGH);
    Serial.println("Hit 2");
  } else {
    Serial.println("Miss...");
  }
}
