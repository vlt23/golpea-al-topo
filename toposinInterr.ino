#include <LiquidCrystal_I2C.h>
#include <Servo.h>
#include <Wire.h>

const int numTopos = 6;

const int pinGolpe1 = 14;
const int pinGolpe2 = 15;
const int pinGolpe3 = 16;
const int pinGolpe4 = 4;
const int pinGolpe5 = 5;
const int pinGolpe6 = 6;

volatile int randNumber;      // elegir que topo aparecer
volatile int golpeYN = 0;     // ver si se ha dado el golpe o no. Tras el golpe correcto, se pone a -1
unsigned long currentTime;    // controlar que el topo aparezca durante dos segundos si no hay ningun golpe
volatile int puntos = 0;

unsigned long gameTime = 123000;   // 2 minuto de juego (+ 3 de delay);
const int pinHit = 7;
const int pinMiss = 3;

const int pinZumbador = 17;

const int servoMov = 0;
const int servoOrig = 20;

// Definicion tipo LCD
// 20 caracteres por linea y 4 lineas
LiquidCrystal_I2C lcd(0x27, 20, 4);

// Servomotor
Servo servo[6];
const int pinServo1 = 8;
const int pinServo2 = 9;
const int pinServo3 = 10;
const int pinServo4 = 11;
const int pinServo5 = 12;
const int pinServo6 = 13;

void setup() {
  pinMode(pinGolpe1, INPUT);
  pinMode(pinGolpe2, INPUT/*_PULLUP*/);
  pinMode(pinGolpe3, INPUT/*_PULLUP*/);
  pinMode(pinGolpe4, INPUT);
  pinMode(pinGolpe5, INPUT);
  pinMode(pinGolpe6, INPUT);

  pinMode(pinServo1, OUTPUT);
  pinMode(pinServo2, OUTPUT);
  pinMode(pinServo3, OUTPUT);
  pinMode(pinServo4, OUTPUT);
  pinMode(pinServo5, OUTPUT);
  pinMode(pinServo6, OUTPUT);

  pinMode(pinZumbador, OUTPUT);

  randomSeed(analogRead(0));
  //attachInterrupt(digitalPinToInterrupt(pinGolpe2), golpeInterrupt2, LOW);
  //attachInterrupt(digitalPinToInterrupt(pinGolpe3), golpeInterrupt3, LOW);
  Serial.begin(9600);

  // LCD
  lcd.init();
  lcd.backlight();
  lcd.home();
  lcd.print("Bienvenido!");
  delay(100);

  // Servo
  for (int i = 0; i < numTopos; i++) {
    servo[i].attach(pinServo1 + i);
  }
  //delay(10000000);
}

void loop() {
  // Fin juego
  if (millis() > gameTime) {
    lcd.clear();
    lcd.setCursor(0, 0);
    lcd.print("Fin juego");
    lcd.setCursor(0, 1);
    lcd.print("Puntuacion: ");
    lcd.print(puntos);
    delay(500);
    music();
  } 
  // En juego
  else {
    imprimirLCD();
    randNumber = (int) random(numTopos);
    Serial.println(randNumber);
    currentTime = millis();
    while (!golpeYN && millis() - 1500 < currentTime) {
      switch (randNumber) {
        case 0:
          servo[0].write(servoMov);
          break;
        case 1:
          servo[1].write(servoMov);
          break;
        case 2:
          servo[2].write(servoMov);
          break;
        case 3:
          servo[3].write(servoMov);
          break;
        case 4:
          servo[4].write(servoMov);
          break;
        case 5:
          servo[5].write(servoMov);
          break;
        case 6:
          servo[6].write(servoMov);
          break;
        default:
          Serial.begin("No deberia entrar aqui");
      }
      // Check sensor golpe
      if (digitalRead(pinGolpe1) == LOW) {
        delay(200);
        if (randNumber == 0) {
          hit(randNumber);
        } else {
          miss();
          Serial.println("Sensor 1");
        }
      }
      if (digitalRead(pinGolpe2) == LOW) {
        delay(200);   // Sensor demasiado sensible...
        if (randNumber == 1) {
          hit(randNumber);
        } else {
          miss();
          Serial.println("Sensor 2");
        }
      }
      if (digitalRead(pinGolpe3) == LOW) {
        delay(200);   // Sensor demasiado sensible...
        if (randNumber == 2) {
          hit(randNumber);
        } else {
          miss();
          Serial.println("Sensor 3");
        }
      }
      if (digitalRead(pinGolpe4) == LOW) {
        delay(200);
        if (randNumber == 3) {
          hit(randNumber);
        } else {
          miss();
          Serial.println("Sensor 4");
        }
      }
      if (digitalRead(pinGolpe5) == LOW) {
        delay(200);
        if (randNumber == 4) {
          hit(randNumber);
        } else {
          miss();
          Serial.println("Sensor 5");
        }
      }
      if (digitalRead(pinGolpe6) == LOW) {
        delay(200);
        if (randNumber == 5) {
          hit(randNumber);
        } else {
          miss();
          Serial.println("Sensor 6");
        }
      }
    }
    // Reiniciar variables para siguiente ronda
    golpeYN = 0;
    digitalWrite(pinHit, LOW);
    digitalWrite(pinMiss, LOW);
    randNumber = -1;
    // Volver posicion inicial los servos
    for (int i = 0; i < numTopos; i++) {
      servo[i].write(servoOrig);
    }
    delay(700);
  }
}

void hit(int randNumber) {
  puntos++;
  golpeYN = 1;
  digitalWrite(pinHit, HIGH);
  Serial.print("Hit ");
  Serial.println(randNumber);
  imprimirLCD();
  tone(pinZumbador, 523.25, 250);
  tone(pinZumbador, 554.37, 250);
}

void miss() {
  puntos--;
  digitalWrite(pinMiss, HIGH);
  Serial.println("Miss...");
  imprimirLCD();
  tone(pinZumbador, 261.63, 250);
}

void imprimirLCD() {
  lcd.setCursor(0, 0);
  lcd.print("Tiempo: ");
  lcd.print((gameTime - millis()));
  lcd.setCursor(0, 1);
  lcd.print("Puntuacion: ");
  lcd.print(puntos);
}

void music() {
  tone(pinZumbador, 293.66, 200);
  delay(200);
  tone(pinZumbador, 293.66, 100);
  delay(100);
  tone(pinZumbador, 293.66, 200);
  delay(200);
  tone(pinZumbador, 293.66, 100);
  delay(100);
  tone(pinZumbador, 293.66, 200);
  delay(200);
  tone(pinZumbador, 293.66, 100);
  delay(100);
  tone(pinZumbador, 293.66, 100);
  delay(100);
  tone(pinZumbador, 293.66, 100);
  delay(100);
  tone(pinZumbador, 293.66, 100);
  delay(100);
  tone(pinZumbador, 293.66, 200);
  delay(200);
  tone(pinZumbador, 293.66, 100);
  delay(100);
  tone(pinZumbador, 293.66, 200);
  delay(200);
  tone(pinZumbador, 293.66, 100);
  delay(100);
  tone(pinZumbador, 293.66, 200);
  delay(200);
  tone(pinZumbador, 293.66, 100);
  delay(100);
  tone(pinZumbador, 293.66, 100);
  delay(100);
  tone(pinZumbador, 293.66, 100);
  delay(100);
  tone(pinZumbador, 293.66, 100);
  delay(100);
  tone(pinZumbador, 293.66, 200);
  delay(200);
  tone(pinZumbador, 293.66, 100);
  delay(100);
  tone(pinZumbador, 293.66, 200);
  delay(200);
  tone(pinZumbador, 293.66, 100);
  delay(100);
  tone(pinZumbador, 293.66, 200);
  delay(200);
  tone(pinZumbador, 293.66, 100);
  delay(100);
  tone(pinZumbador, 293.66, 100);
  delay(100);
  tone(pinZumbador, 440, 100);
  delay(100);
  tone(pinZumbador, 523.25, 100);
  delay(100);
  tone(pinZumbador, 587.33, 100);
  delay(200);
  tone(pinZumbador, 587.33, 100);
  delay(200);
  tone(pinZumbador, 587.33, 100);
  delay(100);
  tone(pinZumbador, 659.25, 100);
  delay(100);
  tone(pinZumbador, 698.45, 100);
  delay(200);
  tone(pinZumbador, 698.45, 100);
  delay(200);
  tone(pinZumbador, 698.45, 100);
  delay(100);
  tone(pinZumbador, 783.99, 100);
  delay(100);
  tone(pinZumbador, 659.25, 100);
  delay(200);
  tone(pinZumbador, 659.25, 100);
  delay(200);
  tone(pinZumbador, 587.33, 100);
  delay(100);
  tone(pinZumbador, 523.25, 100);
  delay(100);
  tone(pinZumbador, 523.25, 100);
  delay(100);
  tone(pinZumbador, 587.33, 100);
  delay(300);
  tone(pinZumbador, 440, 100);
  delay(100);
  tone(pinZumbador, 523.25, 100);
  delay(100);
  tone(pinZumbador, 587.33, 100);
  delay(200);
  tone(pinZumbador, 587.33, 100);
  delay(200);
  tone(pinZumbador, 587.33, 100);
  delay(100);
  tone(pinZumbador, 659.25, 100);
  delay(100);
  tone(pinZumbador, 698.45, 100);
  delay(200);
  tone(pinZumbador, 698.45, 100);
  delay(200);
  tone(pinZumbador, 698.45, 100);
  delay(100);
  tone(pinZumbador, 783.99, 100);
  delay(100);
  tone(pinZumbador, 659.25, 100);
  delay(200);
  tone(pinZumbador, 659.25, 100);
  delay(200);
  tone(pinZumbador, 587.33, 100);
  delay(100);
  tone(pinZumbador, 523.25, 100);
  delay(100);
  tone(pinZumbador, 587.33, 100);
  delay(400);
  tone(pinZumbador, 440, 100);
  delay(100);
  tone(pinZumbador, 523.25, 100);
  delay(100);
  tone(pinZumbador, 587.33, 100);
  delay(200);
  tone(pinZumbador, 587.33, 100);
  delay(200);
  tone(pinZumbador, 587.33, 100);
  delay(100);
  tone(pinZumbador, 698.45, 100);
  delay(100);
  tone(pinZumbador, 783.99, 100);
  delay(200);
  tone(pinZumbador, 783.99, 100);
  delay(200);
  tone(pinZumbador, 783.99, 100);
  delay(100);
  tone(pinZumbador, 880, 100);
  delay(100);
  tone(pinZumbador, 932.33, 100);
  delay(200);
  tone(pinZumbador, 932.33, 100);
  delay(200);
  tone(pinZumbador, 880, 100);
  delay(100);
  tone(pinZumbador, 783.99, 100);
  delay(100);
  tone(pinZumbador, 880, 100);
  delay(100);
  tone(pinZumbador, 587.33, 100);
  delay(300);
  tone(pinZumbador, 587.33, 100);
  delay(100);
  tone(pinZumbador, 659.25, 100);
  delay(100);
  tone(pinZumbador, 698.45, 100);
  delay(200);
  tone(pinZumbador, 698.45, 100);
  delay(200);
  tone(pinZumbador, 783.99, 100);
  delay(200);
  tone(pinZumbador, 880, 100);
  delay(100);
  tone(pinZumbador, 587.33, 100);
  delay(300);
  tone(pinZumbador, 587.33, 100);
  delay(100);
  tone(pinZumbador, 698.45, 100);
  delay(100);
  tone(pinZumbador, 659.25, 100);
  delay(200);
  tone(pinZumbador, 659.25, 100);
  delay(200);
  tone(pinZumbador, 698.45, 100);
  delay(100);
  tone(pinZumbador, 587.33, 100);
  delay(100);
  tone(pinZumbador, 659.25, 100);
  delay(400);
  tone(pinZumbador, 880, 100);
  delay(100);
  tone(pinZumbador, 1046.50, 100);
  delay(100);
  tone(pinZumbador, 1174.66, 100);
  delay(200);
  tone(pinZumbador, 1174.66, 100);
  delay(200);
  tone(pinZumbador, 1174.66, 100);
  delay(100);
  tone(pinZumbador, 1318.51, 100);
  delay(100);
  tone(pinZumbador, 1396.91, 100);
  delay(200);
  tone(pinZumbador, 1396.91, 100);
  delay(200);
  tone(pinZumbador, 1396.91, 100);
  delay(100);
  tone(pinZumbador, 1567.98, 100);
  delay(100);
  tone(pinZumbador, 1318.51, 100);
  delay(200);
  tone(pinZumbador, 1318.51, 100);
  delay(200);
  tone(pinZumbador, 1174.66, 100);
  delay(100);
  tone(pinZumbador, 1046.50, 100);
  delay(100);
  tone(pinZumbador, 1046.50, 100);
  delay(100);
  tone(pinZumbador, 1174.66, 100);
  delay(300);
  tone(pinZumbador, 880, 100);
  delay(100);
  tone(pinZumbador, 1046.50, 100);
  delay(100);
  tone(pinZumbador, 1174.66, 100);
  delay(200);
  tone(pinZumbador, 1174.66, 100);
  delay(200);
  tone(pinZumbador, 1174.66, 100);
  delay(100);
  tone(pinZumbador, 1318.51, 100);
  delay(100);
  tone(pinZumbador, 1396.91, 100);
  delay(200);
  tone(pinZumbador, 1396.91, 100);
  delay(200);
  tone(pinZumbador, 1396.91, 100);
  delay(100);
  tone(pinZumbador, 1567.98, 100);
  delay(100);
  tone(pinZumbador, 1318.51, 100);
  delay(200);
  tone(pinZumbador, 1318.51, 100);
  delay(200);
  tone(pinZumbador, 1174.66, 100);
  delay(100);
  tone(pinZumbador, 1046.50, 100);
  delay(100);
  tone(pinZumbador, 1174.66, 100);
  delay(400);
  tone(pinZumbador, 880, 100);
  delay(100);
  tone(pinZumbador, 1046.50, 100);
  delay(100);
  tone(pinZumbador, 1174.66, 100);
  delay(200);
  tone(pinZumbador, 1174.66, 100);
  delay(200);
  tone(pinZumbador, 1174.66, 100);
  delay(100);
  tone(pinZumbador, 1396.91, 100);
  delay(100);
  tone(pinZumbador, 1567.98, 100);
  delay(200);
  tone(pinZumbador, 1567.98, 100);
  delay(200);
  tone(pinZumbador, 1567.98, 100);
  delay(100);
  tone(pinZumbador, 1760, 100);
  delay(100);
  tone(pinZumbador, 1864.66, 100);
  delay(200);
  tone(pinZumbador, 1864.66, 100);
  delay(200);
  tone(pinZumbador, 1760, 100);
  delay(100);
  tone(pinZumbador, 1567.98, 100);
  delay(100);
  tone(pinZumbador, 1760, 100);
  delay(100);
  tone(pinZumbador, 1174.66, 100);
  delay(300);
  tone(pinZumbador, 1174.66, 100);
  delay(100);
  tone(pinZumbador, 1318.51, 100);
  delay(100);
  tone(pinZumbador, 1396.91, 100);
  delay(200);
  tone(pinZumbador, 1396.91, 100);
  delay(200);
  tone(pinZumbador, 1567.98, 100);
  delay(200);
  tone(pinZumbador, 1760, 100);
  delay(100);
  tone(pinZumbador, 1174.66, 100);
  delay(300);
  tone(pinZumbador, 1174.66, 100);
  delay(100);
  tone(pinZumbador, 1396.91, 100);
  delay(100);
  tone(pinZumbador, 1318.51, 100);
  delay(200);
  tone(pinZumbador, 1318.51, 100);
  delay(200);
  tone(pinZumbador, 1174.66, 100);
  delay(100);
  tone(pinZumbador, 1108.73, 100);
  delay(100);
  tone(pinZumbador, 1174.66, 100);
  delay(200);
  tone(pinZumbador, 1174.66, 100);
  delay(200);
  tone(pinZumbador, 1318.51, 100);
  delay(200);
  tone(pinZumbador, 1396.91, 100);
  delay(200);
  tone(pinZumbador, 1396.91, 100);
  delay(100);
  tone(pinZumbador, 1396.91, 100);
  delay(100);
  tone(pinZumbador, 1567.98, 100);
  delay(200);
  tone(pinZumbador, 1760, 300);
  delay(400);
  tone(pinZumbador, 1396.91, 100);
  delay(100);
  tone(pinZumbador, 1174.66, 100);
  delay(100);
  tone(pinZumbador, 880, 300);
  delay(600);
  tone(pinZumbador, 1864.66, 300);
  delay(400);
  tone(pinZumbador, 1396.91, 100);
  delay(100);
  tone(pinZumbador, 1174.66, 100);
  delay(100);
  tone(pinZumbador, 932.33, 300);
  delay(600);
  tone(pinZumbador, 587.33, 100);
  delay(100);
  tone(pinZumbador, 440, 100);
  delay(200);
  tone(pinZumbador, 587.33, 100);
  delay(300);
  tone(pinZumbador, 554.36, 100);
  delay(400);
  tone(pinZumbador, 1567.98, 100);
  delay(100);
  tone(pinZumbador, 1567.98, 100);
  delay(100);
}
